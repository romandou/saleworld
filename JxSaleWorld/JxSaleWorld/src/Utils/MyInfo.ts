/**
 * Created by ismole on 14-8-8.
 */
class MyInfo  extends egret.Sprite
{
    private static _myInfo:MyInfo = null;
    public static getInstance():MyInfo {

        if(!MyInfo._myInfo){
            MyInfo._myInfo = new MyInfo();
        }
        return MyInfo._myInfo;
    }

    private bgImag:egret.Bitmap;

    public addBgViewHandler():void{
        this.bgImag = GameUtils.createBitmapByName("bgImage");
        this.bgImag.width = 480;
        this.bgImag.height = 800;
        this.bgImag.visible = true;
    }
    public setBgVisible(flag:boolean):void{
        this.bgImag.visible = flag;

    }


}