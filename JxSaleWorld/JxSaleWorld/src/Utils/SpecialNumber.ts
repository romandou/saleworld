/**
 * Created by cf on 14-8-8.
 */
class SpecialNumber extends egret.DisplayObjectContainer
{
    public gap:number = 0;
    public constructor()
    {
       super();
    }

    /**
     * 设置显示的字符串
     */
    public setData(str:string):void{
        this.clear();

        if(str == "" || str == null)
            return;

        var chars:Array<string> = str.split("");
        var w:number = 0;
        var length:number = chars.length;
        for(var i:number = 0;i < length;i++){
            var str:string = chars[i];
            try{
                var image:egret.Bitmap = GameUtils.createBitmapFromSheet(str);
                if(image){
                    image.x = w;
                    w += image.width + this.gap;
                    this.addChild(image);
                }
            }catch(e) {
                console.log(e);
            }

        }
        this.anchorOffsetX = this.width / 2;
    }

    public clear():void{
        while(this.numChildren){
            this.removeChildAt(0);
        }
    }

}