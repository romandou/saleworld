/**
 * Created by lcj on 14-6-17.
 */
class GameUtils {
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     */
    public static createBitmapByName(name:string):egret.Bitmap {
        var result:egret.Bitmap = new egret.Bitmap();
        var texture:egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }

    /**
     * 根据name关键字创建一个Bitmap对象。此name 是根据TexturePacker 组合成的一张位图
     */
    public static createBitmapFromSheet(name:string, sheetName:string = "gameRes"):egret.Bitmap {
        var sheet:egret.SpriteSheet = RES.getRes(sheetName);
        var texture:egret.Texture = sheet.getTexture(name);
        var result:egret.Bitmap = new egret.Bitmap();
        result.texture = texture;
        return result;
    }

    public static getTextureFromSheet(name:string, sheetName:string = "gameRes"):egret.Texture {
        var sheet:egret.SpriteSheet = RES.getRes(sheetName);
        var result:egret.Texture = sheet.getTexture(name);
        return result;
    }


    public static removeChild(child:egret.DisplayObject):void {
        if (child && child.parent) {
            if ((<any>child.parent).removeElement) {
                (<egret.gui.Group><any>child.parent).removeElement(<egret.gui.IVisualElement><any>child);
            }
            else {
                child.parent.removeChild(child);
            }
        }
    }

    public static addUI(ui:egret.gui.IVisualElement):void {
        if (ui && !ui.parent) {
            GameApp.uiStage.addElement(ui);
        }
    }

    public static removeUI(ui:egret.gui.IVisualElement):void {
        if (ui && ui.parent) {
            GameApp.uiStage.removeElement(ui);
        }
    }
    public static removeStarlingSwfUI(ui:egret.DisplayObject):void {
        GameUtils.removeChild(ui);
    }

    public static getUrl(url:string):string {

        return url != "" ? url : "";
    }
    public static addTouchTapListener(display:egret.DisplayObject, listener:Function, thisObj:any):void {
        var startX:number = -1;
        var startY:number = -1;
        display.touchEnabled = true;
        display.addEventListener(egret.TouchEvent.TOUCH_BEGIN, function (event:egret.TouchEvent) {
            startX = event.stageX;
            startY = event.stageY;
        }, display);
        display.addEventListener(egret.TouchEvent.TOUCH_END, function (event:egret.TouchEvent) {
            if (Math.abs(startX - event.stageX) < 10 && Math.abs(startY - event.stageY) < 10) {
                listener.call(thisObj);
            }
            startX = -1;
            startY = -1;
        }, display);
    }

    public static fixNumber(num:number):string {
        var result:string = "";
        if (num > 10000) {
            num = Math.floor(num / 10000);
            result = num.toString() + "w";
        }
        else {
            result = num.toString();
        }
        return result;
    }

    public static RandomIntRange(iMin:number, iMax:number):number
    {
        var iLength = iMax - iMin;
        var iResult = Math.random() * iLength;
        return Math.floor(iMin + iResult); 
    }

    public static lock(key:string):void {
        var stage:egret.Stage = egret.MainContext.instance.stage;
        stage.touchEnabled = stage.touchChildren = false;
    }

    public static unlock(key:string):void {
        //todo key
        var stage:egret.Stage = egret.MainContext.instance.stage;
        stage.touchEnabled = stage.touchChildren = true;
    }


    public static replaceTextureByUrl(url:string,obj:egret.Bitmap) {
        RES.getResByUrl(url, function (texture):void {
            obj.texture = texture;
        }, this, RES.ResourceItem.TYPE_IMAGE);
    }

    public static getIsTodayFirstIn():boolean {
        var date:Date = new Date();
        var lastDate:string = window.localStorage.getItem("LoginDate");
        if(lastDate == null) {
            lastDate = date.getMonth().toString() + date.getDay().toString();
            window.localStorage.setItem("LoginDate", lastDate);
            return true;
        }
        else {
            var currentDate = date.getMonth().toString() + date.getDay().toString();
            window.localStorage.setItem("LoginDate", currentDate);
            return currentDate != lastDate;
        }
    }
}