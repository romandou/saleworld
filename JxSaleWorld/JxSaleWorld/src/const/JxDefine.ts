class JxDefine {
	public constructor() {
	}
}

class TGoodsInfo
{
	public iItemID :number;
	public iRealPrice  :number;
	public fRoom   :number;
	public iRestCount : number;
	public strName :string;
}
enum EOptionType
{
	eOption_Main,
	eOption_Skill,
	eOption_Friend,
	eOption_Other,
	eOption_Count
}

enum ColorType
{
	eColor_Black = 0x000000,
	eColor_Blue = 0x0000FF,
	eColor_Green = 0x00FF00,
	eColor_Red = 0xFF00000,
	eColor_White = 0XFFFFFF
}

class TRegion
{
	public X:number = 0;
	public Y:number = 0;
	public W:number = 0;
	public H:number = 0;
	public constructor(x:number, y:number, w:number, h:number)
	{
		this.X = x;
		this.Y = y;
		this.W = w;
		this.H = h;
	}
}
class TSellingItem
{
	public iItemId:number = 0;
	public iCount:number = 0;
	public fLastSellTime:number = 0;
	public kPriceList:Array<number>;

}
