// TypeScript file
// TypeScript file
class TItemInfo extends egret.Sprite
{
	public constructor() 
	{
		super();
	}
	private m_kButton:JxUIButton;
	private m_iItemID:number;
	private static FONTSIZE:number = 20;
	public init(iItemID:number, iIndex:number):void 
	{
		this.m_kButton = new JxUIButton("img_sm_item"+ iItemID, "goods"+ iItemID);
		this.m_kButton.x = 0;
		this.m_kButton.y = 0;
		this.m_kButton.width = 50;
		this.m_kButton.height = 50;
		this.m_kButton.setClick(this.OnChoose, iItemID, this);
		this.addChild(this.m_kButton);
	}
	public Refresh():void
	{
/*		this.m_kContainLabel.text =  "余:" +  this.m_kGoodsInfo.iRestCount;
		if(this.m_kGoodsInfo.iRestCount == 0)
		{
			var colorMatrix = [
    			0.3,0.6,0,0,0,
    			0.3,0.6,0,0,0,
    			0.3,0.6,0,0,0,
    			0,0,0,1,0
			];
			var colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
			this.m_kButton.bg.filters = [colorFlilter];
			return;
		}
		var bEnough:boolean = GameApp.s_GameCore.m_kPlayer.CheckMoney(this.m_kGoodsInfo.iRealPrice);
		var iColor = ColorType.eColor_White;
		if (bEnough == false)
			iColor = ColorType.eColor_Red;
		this.m_kPriceLabel.textColor = iColor;
        */
	}
	public OnChoose(iItemID:number , oneGrid:any) :void 
	{
        JxGoodsPage.getInstance().OnChooseItem(iItemID);
	}
}

class JxItemGrid extends egret.Sprite
 {
	public constructor() 
	{
		super();
		this.init();
	}
	private m_kItemList:Array<TItemInfo>;
	public init():void
	{
		this.m_kItemList = new Array<TItemInfo>();
		for (var i = 0; i < JxGameConfig.m_kGameConfig.length; i ++)
			{
                var jj = Math.floor(i / 5);
                var ii = i - jj * 5;
				var Goods = new TItemInfo();
				Goods.x = ii * 50;
				Goods.y = jj * 50;
				Goods.width  = 50;
				Goods.height = 50;
				var kGoodsInfo = JxGameConfig.m_kGameConfig[i];
				Goods.init(kGoodsInfo.id, i);
				Goods.touchEnabled = true;
				this.touchEnabled = true;
				this.m_kItemList.push(Goods);
				this.addChild(Goods);
			}
	}
}
class JxGoodsPage extends JxUIPage{
    public constructor()
    {
        super();
        this.Init();
        this.OnChooseItem(1);
    }
    private static _Single:JxGoodsPage = null;
    public static getInstance():JxGoodsPage {

        if(!JxGoodsPage._Single)
        {
            JxGoodsPage._Single = new JxGoodsPage();
        }
        return JxGoodsPage._Single;
    }

    private m_kItemGrid:JxItemGrid;
    private m_kLableName:egret.TextField;
    private m_kLableDesc:egret.TextField;
    private m_kItemIcon:egret.Bitmap;
    private m_kBuyButton:JxUIButton;
    private m_kLableInfo:egret.TextField;
    private m_iCurChooseItem:number;
    private m_kInfoBackGround:egret.Bitmap;
    public Init()
    {
        
        var m_kInfoBackGround:egret.Bitmap = GameUtils.createBitmapFromSheet("img_uiboard");
        var rect:egret.Rectangle = new egret.Rectangle(18,5,15,5);
        m_kInfoBackGround.scale9Grid = rect;
        m_kInfoBackGround.width = 160;//230;
        m_kInfoBackGround.height = 500;
        m_kInfoBackGround.x = 260;
        m_kInfoBackGround.y = 0;
        m_kInfoBackGround.alpha = 0.7;
        this.addChild(m_kInfoBackGround);
        
        this.m_kItemGrid = new JxItemGrid();
        this.addChild(this.m_kItemGrid);
        this.m_kLableName = new egret.TextField();
        this.addChild(this.m_kLableName);
        this.m_kLableName.x = 390;
        this.m_kLableName.y = 45;
        this.m_kLableName.size = 10;
        this.m_kLableName.height = 20;
        this.m_kLableName.text = "itemname"

        this.m_kLableDesc = new egret.TextField();
        this.addChild(this.m_kLableDesc);
        this.m_kLableDesc.text = "desc desc des \n dsesdlfsj\nasdflkjasdfjlalsdfjs\nasldkfjalsjdf\asdfasdfjdsa";
        this.m_kLableDesc.x = 270;
        this.m_kLableDesc.y = 110;
        this.m_kLableDesc.size = 10;
        this.m_kLableDesc.height = 100;
        this.m_kItemIcon = GameUtils.createBitmapFromSheet("img_sm_item28");
        this.m_kItemIcon.x = 265;
        this.m_kItemIcon.y = 2;
        //this.m_kItemIcon.width = 200;
        //this.m_kItemIcon.height = 400;
        this.addChild(this.m_kItemIcon );
        
        this.m_kBuyButton = new JxUIButton("img_uibutton","");
        this.m_kBuyButton.x = 360;
        this.m_kBuyButton.y = 430;
        this.addChild(this.m_kBuyButton);
        this.m_kBuyButton.visible = false;
    }

    public OnChooseItem(iItemId:number)
    {
        var data = JxGameConfig.m_kGameConfig[iItemId - 1];
        
        this.m_kLableName.text = data.name;
        this.m_kLableDesc.text = data.desc;
        this.m_kItemIcon.texture = GameUtils.getTextureFromSheet(data.img);
    }



    }