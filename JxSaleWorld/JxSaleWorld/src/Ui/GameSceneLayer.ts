/**
 * 游戏场景
 * Created by ismole on 14-8-8.
 */
class GameSceneLayer extends egret.Sprite {
    public constructor() {
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE,this.init,this);
    }
    private static _Single:GameSceneLayer = null;
    public static getInstance():GameSceneLayer {

        if(!GameSceneLayer._Single)
        {
            GameSceneLayer._Single = new GameSceneLayer();
        }
        return GameSceneLayer._Single;
    }

    private m_kOptionButtons:Array<JxUIButton>;
    private m_kBottomLine:egret.Shape;
    private m_iMoneyCount:number = 1000;
    private m_iContainCount:number = 0;
    private m_iMaxContainCount:number = 100;
    private m_kLabelCurMoney : egret.TextField;
    private m_kLabelPlayerInfo : egret.TextField;
    private m_kRoomProcessBar: JxUIProcessBar;
    private m_kExpProcessBar: JxUIProcessBar;
    public m_bInGame:boolean = false;
    public m_kGameTips:egret.TextField;
    private m_kSound:egret.Sound;
    private m_kPageList:JxUIPageList;

    private init() : void 
    {
        
        //var kBackRegion = new TRegion(0,0, Consts.GAME_WIDTH, Consts.GAME_HEIGHT);

        //var line = JxShaperHelper.CreateFillRegion(kBackRegion, ColorType.eColor_Blue);
        //this.addChild(line);
        this.m_kLabelPlayerInfo = new egret.TextField();
        this.m_kLabelPlayerInfo.text = "名:" + GameApp.s_GameCore.m_kPlayer.m_szName + "\nLv:" + GameApp.s_GameCore.m_kPlayer.m_iLevel;
        this.m_kLabelPlayerInfo.x = 10;
        this.m_kLabelPlayerInfo.y = 10;
        this.m_kLabelPlayerInfo.size = 20;
		this.m_kLabelPlayerInfo.textColor = ColorType.eColor_Black; 
        this.addChild(this.m_kLabelPlayerInfo);

        this.m_kLabelCurMoney = new egret.TextField();
        this.m_kLabelCurMoney.text = "钱:" + GameApp.s_GameCore.m_kPlayer.m_iMoney;
        this.m_kLabelCurMoney.x = 400;
        this.m_kLabelCurMoney.y = 40;
        this.m_kLabelCurMoney.size = 20;
		this.m_kLabelCurMoney.textColor = ColorType.eColor_Black; 
        this.addChild(this.m_kLabelCurMoney);

        this.m_kRoomProcessBar = new JxUIProcessBar(0, GameApp.s_GameCore.m_kPlayer.m_iMaxRoomCount, ColorType.eColor_Green, true);
        //this.m_kLabelRoomInfo = new egret.TextField();
        //this.m_kLabelRoomInfo.text = "库:" + GameApp.s_GameCore.m_kPlayer.m_iCurRoomCount + "/" + GameApp.s_GameCore.m_kPlayer.m_iMaxRoomCount;
        this.m_kRoomProcessBar.x = 200;
        this.m_kRoomProcessBar.y = 40;
        this.m_kRoomProcessBar.width = 100;
        this.m_kRoomProcessBar.height = 20;
        //this.m_kRoomProcessBar = 20;
		//this.m_kRoomProcessBar.textColor = ColorType.eColor_Black;
       // this.addChild(this.m_kRoomProcessBar);
       
        this.m_kExpProcessBar = new JxUIProcessBar(0, GameApp.s_GameCore.m_kPlayer.m_iNextLevelExp, ColorType.eColor_Blue, false);
        //this.m_kLabelRoomInfo = new egret.TextField();
        //this.m_kLabelRoomInfo.text = "库:" + GameApp.s_GameCore.m_kPlayer.m_iCurRoomCount + "/" + GameApp.s_GameCore.m_kPlayer.m_iMaxRoomCount;
        this.m_kExpProcessBar.x = 0;
        this.m_kExpProcessBar.y = 790;
        this.m_kExpProcessBar.width = 470;
        this.m_kExpProcessBar.height = 6;
        //this.m_kRoomProcessBar = 20;
		//this.m_kRoomProcessBar.textColor = ColorType.eColor_Black;
        this.addChild(this.m_kExpProcessBar);

        
        this.m_kOptionButtons = new Array<JxUIButton>();
        for (var i = 0; i < EOptionType.eOption_Count; i ++)
        {
            var button:JxUIButton = new  JxUIButton("bt_button" + (i+1), "sale");
            button.x = i * Consts.GAME_WIDTH / EOptionType.eOption_Count;
            button.y = Consts.GAME_HEIGHT - 137;
            button.setClick(this.OnOptionClick, i);
            this.m_kOptionButtons.push(button);
            this.addChild(button);
        }
        this.m_kPageList = new JxUIPageList();
        this.m_kPageList.x = 40;
        this.m_kPageList.y = 100;
        this.m_kPageList.width = 400;
        this.m_kPageList.height = 400;
        var kMainPage:JxMainPage = new JxMainPage();
        this.m_kPageList.AddPage(kMainPage);
        var kGoodsPage:JxGoodsPage = JxGoodsPage.getInstance();
        this.m_kPageList.AddPage(kGoodsPage);
        this.m_kPageList.ActivePage(0);
        this.addChild(this.m_kPageList);
        

        var kRegion = new TRegion(0,Consts.GAME_HEIGHT - 140, Consts.GAME_WIDTH, 3);

        var line = JxShaperHelper.CreateFillRegion(kRegion, ColorType.eColor_Red);
        this.addChild(line);

        this.m_kGameTips = new egret.TextField();
        this.m_kGameTips.text = "";
        this.m_kGameTips.x = 30;
        this.m_kGameTips.y = 100;
        this.m_kGameTips.size = 20;
		this.m_kGameTips.textColor = ColorType.eColor_Blue; 
        this.addChild(this.m_kGameTips);
        /*var m_kSound = new egret.Sound();
        m_kSound.addEventListener(egret.Event.COMPLETE, function loadOver(event:egret.Event)
         {
            m_kSound.play();
        }, this);
  m_kSound.addEventListener(egret.IOErrorEvent.IO_ERROR, function loadError(event:egret.IOErrorEvent) {
      console.log("loaded error!");
  }, this);


     m_kSound.load("resource/sound/sound.mp3");
     */
        this.m_bInGame = true;
    }

    public AddProfitInfo(iProfit:number):void
    {
        this.m_kPageList.SendEvent(1, iProfit);
    } 
    public ShowTips(strMsg:string , fShowTime:number)
    {
        this.m_kGameTips.text = strMsg;
        this.m_kGameTips.visible = true;
        var timer:egret.Timer = new egret.Timer(fShowTime * 1000,1);
       //注册事件侦听器
        timer.addEventListener(egret.TimerEvent.TIMER,this.EndTips,this);
        //timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE,this.timerComFunc,this);
        //开始计时
        timer.start();
    }

   public RefreshGrid():void
    {
        var kMainPage  =  < JxMainPage > this.m_kPageList.GetPage(0);

        kMainPage.RefreshGrid();
    }
    
    public EndTips()
    {
        this.m_kGameTips.text = "";
        this.m_kGameTips.visible = false;
    }
    public Refresh():void 
    {

    if (this.m_bInGame)
        {
            this.m_kRoomProcessBar.ChangeCurValue( GameApp.s_GameCore.m_kPlayer.m_iCurRoomCount);
            this.m_kRoomProcessBar.ChangeMaxValue( GameApp.s_GameCore.m_kPlayer.m_iMaxRoomCount);
            this.m_kRoomProcessBar.Refresh();
            this.m_kExpProcessBar.ChangeCurValue( GameApp.s_GameCore.m_kPlayer.m_iCurExp);
            this.m_kExpProcessBar.ChangeMaxValue( GameApp.s_GameCore.m_kPlayer.m_iNextLevelExp);
            this.m_kExpProcessBar.Refresh();
            this.m_kLabelCurMoney.text = "钱:" + GameApp.s_GameCore.m_kPlayer.m_iMoney;
            this.m_kLabelPlayerInfo.text = "名:" + GameApp.s_GameCore.m_kPlayer.m_szName + "\nLv:" + GameApp.s_GameCore.m_kPlayer.m_iLevel;
            //this.m_kLabelPriftInfo.text = "本轮收益:" + GameApp.s_GameCore.m_kPlayer.m_iCurrentTotalProft + "\n 历史最高:" + GameApp.s_GameCore.m_kPlayer.m_kMaxProfit;
        }
        //this.m_kLabelRoomInfo.text = "库:" + GameApp.s_GameCore.m_kPlayer.m_iCurRoomCount + "/" + GameApp.s_GameCore.m_kPlayer.m_iMaxRoomCount;
    }

    public ActivePage(iPageIndex:number)
    {
        this.m_kPageList.ActivePage(iPageIndex);
    }

    public OnUpdate()
    {
        this.m_kPageList.OnUpdate(0);
    }
    
    private OnOptionClick(iButtonIndex:number):void 
    {
        GameSceneLayer.getInstance().ActivePage(iButtonIndex);
    }
}
