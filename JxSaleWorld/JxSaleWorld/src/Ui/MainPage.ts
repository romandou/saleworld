// TypeScript file
class JxMainPage extends JxUIPage{
    public constructor()
    {
        super();
        this.init();
    }

    private m_kGoodGrid:JxGoodsGrid;
    private m_kProfitTipsList:Array<egret.TextField>;
    private m_kLabelPriftInfo:egret.TextField;

    private init() : void 
    {
        this.m_kProfitTipsList = new Array<egret.TextField>();
        this.m_kLabelPriftInfo = new egret.TextField();
        this.m_kLabelPriftInfo.text = "";
        this.m_kLabelPriftInfo.x = 20;
        this.m_kLabelPriftInfo.y = 60;
        this.m_kLabelPriftInfo.size = 20;
		this.m_kLabelPriftInfo.textColor = ColorType.eColor_Black; 
        this.addChild(this.m_kLabelPriftInfo);

        this.m_kGoodGrid = new JxGoodsGrid();
        this.m_kGoodGrid.x = 0;
        this.m_kGoodGrid.y = 0;
        this.m_kGoodGrid.width = 400;
        this.m_kGoodGrid.height = 400;
        this.m_kGoodGrid.touchEnabled = true;
        this.addChild(this.m_kGoodGrid);
        this.touchEnabled = true;
    }
    public AddProfitInfo(iProfit:number):void 
    {
        if (this.m_kProfitTipsList.length > 20)
        {
            var kOldText = this.m_kProfitTipsList.shift();
            this.removeChild(kOldText);
        }
            var kNewText = new egret.TextField();
            kNewText.x = GameUtils.RandomIntRange(10, 400);
            kNewText.y = 640;
            if (iProfit > 0)
                kNewText.text = "+" + iProfit;
            else
                kNewText.text = "" + iProfit;
                
            this.m_kProfitTipsList.push(kNewText);
            this.addChild(kNewText);
    }
   public OnUpdate()
    {
        for (var kProfitText of this.m_kProfitTipsList)
        {
            if (kProfitText.y <= 500)
            {
                kProfitText.visible = false;
            }
            else
            {
                kProfitText.y -= 1;
            }
        }
    }
   public RefreshGrid():void
    {
        this.m_kGoodGrid.Refresh();
    }
    
    public OnReceveEvent(iEventType:number, iParam1:number = 0, iParam2:number = 0)
    {
        if (iEventType == 1)
        {
            this.AddProfitInfo(iParam1);
        }
    }
}