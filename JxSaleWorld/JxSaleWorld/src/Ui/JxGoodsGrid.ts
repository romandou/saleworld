class TOneGoods extends egret.Sprite
{
	public constructor() 
	{
		super();
	}
	private m_kButton:JxUIButton;
	private m_kPriceLabel:egret.TextField;
	private m_kContainLabel:egret.TextField;
	private m_kOcupyLabel:egret.TextField;
	private m_kIntervalLabel:egret.TextField;
	private m_kGoodsInfo:TGoodsInfo;
	private static FONTSIZE:number = 20;
	public m_kProfitText:egret.TextField = null;
	
	public init(kGoodsInfo:TGoodsInfo, iIndex:number):void 
	{
		this.m_kGoodsInfo = kGoodsInfo;
		this.m_kButton = new JxUIButton("img_item"+ kGoodsInfo.iItemID, "goods"+ kGoodsInfo.iItemID);
		this.m_kButton.x = 0;
		this.m_kButton.y = 0;
		this.m_kButton.width = 100;
		this.m_kButton.height = 100;
		this.m_kButton.setClick(this.OnSale, iIndex, this);
		this.addChild(this.m_kButton);

		this.m_kPriceLabel = new egret.TextField();
		this.m_kPriceLabel.text =  "价:" + kGoodsInfo.iRealPrice ;
		this.m_kPriceLabel.x = 0;
		this.m_kPriceLabel.y = 70;
		this.m_kPriceLabel.size = TOneGoods.FONTSIZE;
	//	this.m_kPriceLabel.height = 10;
		this.m_kPriceLabel.textColor = ColorType.eColor_White; 
		this.addChild(this.m_kPriceLabel);
		
		this.m_kIntervalLabel = new egret.TextField();
		this.m_kIntervalLabel.text =  "时:" +  JxGameConfig.m_kGameConfig[kGoodsInfo.iItemID].selltime;
		this.m_kIntervalLabel.x = 70;
		this.m_kIntervalLabel.y = 20;
		this.m_kIntervalLabel.size = TOneGoods.FONTSIZE;
		this.m_kIntervalLabel.textColor = ColorType.eColor_White; 
		//this.m_kIntervalLabel.height = 10;
	//	this.addChild(this.m_kIntervalLabel);

		this.m_kOcupyLabel = new egret.TextField();
		this.m_kOcupyLabel.text =  "容:" + kGoodsInfo.fRoom;
		this.m_kOcupyLabel.x = 70;
		this.m_kOcupyLabel.y = 50;
		this.m_kOcupyLabel.size = TOneGoods.FONTSIZE;
		this.m_kOcupyLabel.textColor = ColorType.eColor_White; 
		//this.m_kOcupyLabel.height = 10;
	//	this.addChild(this.m_kOcupyLabel);
		
		this.m_kContainLabel = new egret.TextField();
		this.m_kContainLabel.text =  "余:" +  kGoodsInfo.iRestCount;
		this.m_kContainLabel.x = 70;
		this.m_kContainLabel.y = 90;
		this.m_kContainLabel.size = TOneGoods.FONTSIZE;
		this.m_kContainLabel.textColor = ColorType.eColor_White; 
		//this.m_kContainLabel.height = 10;
	//	this.addChild(this.m_kContainLabel);
		this.m_kProfitText = new egret.TextField();
		this.m_kProfitText.text = "";
		this.m_kProfitText.x = 10;
		this.m_kProfitText.y = 50;
		this.m_kProfitText.width = 90;
		this.m_kProfitText.height = 30;
		//this.m_kProfitText.visible = false;
		this.addChild(this.m_kProfitText);
		

	}
	public Refresh():void
	{
		this.m_kContainLabel.text =  "余:" +  this.m_kGoodsInfo.iRestCount;
		if(this.m_kGoodsInfo.iRestCount == 0)
		{
			var colorMatrix = [
    			0.3,0.6,0,0,0,
    			0.3,0.6,0,0,0,
    			0.3,0.6,0,0,0,
    			0,0,0,1,0
			];
			var colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
			this.m_kButton.bg.filters = [colorFlilter];
			return;
		}
		var bEnough:boolean = GameApp.s_GameCore.m_kPlayer.CheckMoney(this.m_kGoodsInfo.iRealPrice);
		var iColor = ColorType.eColor_White;
		if (bEnough == false)
			iColor = ColorType.eColor_Red;
		this.m_kPriceLabel.textColor = iColor;
	}


	public OnSale(iItemIndex:number , oneGrid:any) :void 
	{
		if (oneGrid.m_kGoodsInfo == null)
			return;
		
		var profit = GameApp.s_GameCore.m_kPlayer.BuyGoods(iItemIndex);
		if (profit != 0)
		{
			if (profit > 0)
			{
				oneGrid.m_kProfitText.text = "+" + profit;
				oneGrid.textColor = ColorType.eColor_Green;
			}
			else
			{
				oneGrid.m_kProfitText.text = "" + profit;
				oneGrid.textColor = ColorType.eColor_Red;
			}

	        oneGrid.visible = true;

			GameControl.getInstance().Refresh();
			oneGrid.Refresh();
		}
	}
}

class JxGoodsGrid extends egret.Sprite
 {
	public constructor() 
	{
		super();
		this.init();
	}
	private m_kSaleList:Array<TOneGoods>;
	private init():void
	{
		this.m_kSaleList = new Array<TOneGoods>();
		for (var i = 0; i < Consts.GAME_SHOPGRID_WIDTH; i ++)
			for (var j = 0; j < Consts.GAME_SHOPGRID_HEIGHT; j ++)
			{
				var Goods = new TOneGoods();
				Goods.x = i * 100;
				Goods.y = j * 100;
				Goods.width  = 100;
				Goods.height = 100;
				var kGoodsInfo = GameApp.s_GameCore.m_kPlayer.m_kGoodsList[j* Consts.GAME_SHOPGRID_WIDTH + i];
				Goods.init(kGoodsInfo, j*Consts.GAME_SHOPGRID_WIDTH + i);
				Goods.touchEnabled = true;
				this.touchEnabled = true;
				this.m_kSaleList.push(Goods);
				this.addChild(Goods);

			}
	}
	public Refresh():void
	{
		for (var i = 0; i < Consts.GAME_SHOPGRID_WIDTH; i ++)
			for (var j = 0; j < Consts.GAME_SHOPGRID_HEIGHT; j ++)
			{
				var Goods = this.m_kSaleList[j* Consts.GAME_SHOPGRID_WIDTH + i];
				Goods.x = i * 100;
				Goods.y = j * 100;
				Goods.width  = 100;
				Goods.height = 100;
				var kGoodsInfo = GameApp.s_GameCore.m_kPlayer.m_kGoodsList[j* Consts.GAME_SHOPGRID_WIDTH + i];
				Goods.removeChildren();
				Goods.init(kGoodsInfo, j*Consts.GAME_SHOPGRID_WIDTH + i);
				Goods.touchEnabled = true;
				this.touchEnabled = true;
				//this.m_kSaleList.push(Goods);
				//this.addChild(Goods);

			}
		
		
	}

}