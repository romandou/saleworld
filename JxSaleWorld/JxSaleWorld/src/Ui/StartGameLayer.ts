/**
 * 开始游戏界面
 * Created by cf on 14-8-8.
 */
class StartGameLayer extends egret.Sprite
{
    private startBtn:JxUIButton
    private moreBtn:JxUIButton
    private titleImage:egret.Bitmap;
    private  bgImag:egret.Bitmap;
    public constructor()
    {
        super();
        this.init();

    }
    private init():void
    {

     //   this.bgImag = GameUtils.createBitmapFromSheet("img_back");
       // this.bgImag.width = 480;
       // this.bgImag.height = 800;
        //this.addChild(this.bgImag );

//        this.titleImage = GameUtils.createBitmapFromSheet("logo_mishitaosheng");
  //      this.titleImage.x = 51;
    //    this.titleImage.y = 161;
      //  this.addChild(this.titleImage);

        this.startBtn = new JxUIButton("bt_login","bt_login");
        this.startBtn.x =131;
        this.startBtn.y =500;
        this.startBtn.setClick(this.onStartGameClick)
        this.addChild(this.startBtn);

        //this.moreBtn = new JxUIButton("btn_b" ,"btn_gengduo");
        //this.moreBtn.setClick(function(){EgretShare.moreGame()});

        //this.moreBtn.x =131;
        //this.moreBtn.y =432;
        if(egret.MainContext.RUNTIME_HTML5 == egret.Capabilities.runtimeType)
            this.addChild(this.moreBtn);

        var txt:egret.TextField = new egret.TextField();
        txt.width = Consts.GAME_WIDTH;
        txt.textAlign = egret.HorizontalAlign.CENTER;
        txt.strokeColor = 0x403e3e;
        txt.stroke = 1;
        txt.bold = true;
        txt.y = 612;
        txt.text = "Made By JX ";
        this.addChild(txt);
    }

    private onStartGameClick():void
    {
        GameControl.getInstance().onGameSceneHandler();
    }
}