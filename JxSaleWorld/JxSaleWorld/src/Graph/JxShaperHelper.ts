class JxShaperHelper {
	public constructor() {
	}

	public static CreateFillRegion(kRegion:TRegion, iColor:number):egret.Shape
	{
		var topLine = new egret.Shape();
		topLine.graphics.beginFill(iColor, 1);
		topLine.graphics.drawRect(kRegion.X, kRegion.Y, kRegion.W, kRegion.H);
		topLine.graphics.endFill();
		return topLine;
	}
}