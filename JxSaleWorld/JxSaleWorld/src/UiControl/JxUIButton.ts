/**
 * Created by Administrator on 2014/8/8.
 */
class JxUIButton extends egret.Sprite{
    private _bg:egret.Bitmap;
    private title:egret.Bitmap;
    private onClick:Function;
    private iClickParam1:number;
    private iClickParam2:any;
    private kText:egret.TextField;
    public constructor(bgName:string, titleName:string){
        super();
        this._bg = GameUtils.createBitmapFromSheet(bgName);
        this.addChild(this._bg);

        this.title = GameUtils.createBitmapFromSheet(titleName);
        if(this.title.texture == null){
            this.title.texture = RES.getRes(titleName);
        }
        this.title.x = (this._bg.width - this.title.width) >> 1;
        this.title.y = (this._bg.height - this.title.height) >> 1;
        this.addChild(this.title);
    }
    public SetText(szText:string, iColor:number = ColorType.eColor_Blue)
    {
        if (this.kText == null)
        {
            this.kText = new egret.TextField();
            this.kText.x = this.width * 0.1;
            this.kText.y = this.height * 0.1;
            this.kText.width = this.width*0.8;
            this.kText.height = this.height * 0.8;
            this.kText.textColor = iColor;
            this.addChild(this.kText);
        }
        this.kText.text = szText; 
    }

    public setClick(func:Function, param1:number = 0, param2:any = null):void{
        this.touchEnabled = true;
        this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onClickEvent, this);
        this.onClick = func;
        this.iClickParam1 = param1;
        this.iClickParam2 = param2;
    }

    private onClickEvent():void
    {
        this.onClick(this.iClickParam1, this.iClickParam2);
    }

    public setTitle(title:string):void{
        this.title = GameUtils.createBitmapFromSheet(title);
    }

    public ChangeTexture(kTexture:egret.Texture)
    {
        this._bg.texture = kTexture;
    }
    public get bg() {
        return this._bg;
    }
    public set bg(bg:egret.Bitmap) {
        this._bg = bg;
    }
}