// TypeScript file
class JxUIPage extends egret.Sprite{
    private m_iPageIndex:number;
    public constructor()
    {
        super();
    }

    public SetPageIndex(iPageIndex:number)
    {
        this.m_iPageIndex = iPageIndex;
    }
    
    public OnUpdate(fCurrentTime:number)
    {

    }
    public OnReceveEvent(iEventType:number, iParam1:number = 0, iParam2:number = 0):void
    {

    }
}