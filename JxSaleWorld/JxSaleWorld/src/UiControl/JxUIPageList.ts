// TypeScript file
class JxUIPageList extends egret.Sprite{
    private m_iActivePageIndex:number;
    private m_kPageList:Array<JxUIPage>;
    public constructor()
    {
        super();
        this.m_iActivePageIndex = -1;
        this.m_kPageList = new Array<JxUIPage>();
    }
    public AddPage(kPage:JxUIPage):void
    {
        this.m_kPageList.push(kPage);
        kPage.SetPageIndex(this.m_kPageList.length - 1);
    }
    public OnUpdate(fCurrentTime:number)
    {
        if (this.m_iActivePageIndex >= 0)
        {
            this.m_kPageList[this.m_iActivePageIndex].OnUpdate(fCurrentTime);
        }

    }
    
    public GetPage(iPageIndex:number):JxUIPage
    {
        if (iPageIndex < 0 || iPageIndex >= this.m_kPageList.length)
            return null;
        return this.m_kPageList[iPageIndex];
    }

    public GetActivePage():JxUIPage
    {
        if (this.m_iActivePageIndex < 0)
            return null;
        return this.m_kPageList[this.m_iActivePageIndex];
    }

    public SendEvent(iEventType:number, iParam1:number = 0, iParam2:number = 0)
    {
        var kPage = this.GetActivePage();
        if (kPage != null)
        {
            kPage.OnReceveEvent(iEventType, iParam1, iParam2);
        }
    }

    public ActivePage(iPageIndex:number):void
    {
        if (iPageIndex >= this.m_kPageList.length)
            return ;

        if (this.m_iActivePageIndex == iPageIndex)
            return ;
        
        if (this.m_iActivePageIndex != -1)
            this.removeChild(this.m_kPageList[this.m_iActivePageIndex]);
        
        this.addChild(this.m_kPageList[iPageIndex]);

        this.m_iActivePageIndex = iPageIndex;
    }

}