class JxUIProcessBar extends egret.Sprite{
    private _bgRegion:egret.Bitmap;
	private _bg:egret.Bitmap;
	private m_iMaxValue:number;
	private m_iCurValue:number;
	private m_bShowText:boolean;
	private _bgFill:egret.Shape;
	private m_iColor:number;
	private m_kText:egret.TextField;


    public constructor(iCurValue:number, iMaxValue:number, iBgColor:number=ColorType.eColor_Red, bShowText:boolean = false, bgName:string = "", bgRegionName:string = "")
	{
        super();
		this._bg = null;
		this._bgRegion = null;
		this.m_iCurValue = iCurValue;
		this.m_iMaxValue = iMaxValue;
		this.m_iColor = iBgColor;
		this.m_kText = null;
		if (bgName != "")
        {
			this._bg = GameUtils.createBitmapFromSheet(bgName);
        	this.addChild(this._bg);
		}
		else
		{
			this._bgFill = new egret.Shape();
			this.addChild(this._bgFill);
		}

		if (bgRegionName != "")
        {
			this._bgRegion = GameUtils.createBitmapFromSheet(bgName);
        	this.addChild(this._bgRegion);
		}

		if (bShowText)
		{
			this.m_kText = new egret.TextField();
			this.addChild(this.m_kText); 
		}
    }
	public ChangeCurValue(iCurValue:number):void
	{
		this.m_iCurValue = iCurValue;
	}
	public ChangeMaxValue(iMaxValue:number):void
	{
		this.m_iMaxValue = iMaxValue;
	}

	public Refresh()
	{
		var fPercent = this.m_iCurValue / this.m_iMaxValue;
		if (fPercent > 1)
			fPercent = 1;
		var ibgwidth = fPercent * this.width;
		if (this._bg)
		{
			this._bg.width = ibgwidth;
		}
		else
		{
			this._bgFill.graphics.clear();
			this._bgFill.graphics.beginFill( this.m_iColor, 1);
        	this._bgFill.graphics.drawRect( 0, 0, ibgwidth, this.height );
        	this._bgFill.graphics.endFill();
		}
		
		if (this.m_kText)
		{
			this.m_kText.text = this.m_iCurValue + "/" + this.m_iMaxValue;
		}
	}
}