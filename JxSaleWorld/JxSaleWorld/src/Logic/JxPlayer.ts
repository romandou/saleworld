class JxPlayer {
	public constructor() {
		this.m_kValidBuyItem = new Array<number>();
		this.m_kGoodsList = new Array<TGoodsInfo>();
		this.m_kBuyedGoodsList = new Map<number,TSellingItem>();
	}

	public m_szName:string = "J&X";
	public m_iMoney:number = 0;
	public m_iLevel:number = 1;
	public m_iCurRoomCount = 0;
	public m_iMaxRoomCount = 100;
	public m_kGoodsList:Array<TGoodsInfo>;
	public m_kValidBuyItem:Array<number>;
	public m_kBuyedGoodsList : Map<number,TSellingItem>;
	public m_iCurExp:number		= 0;
	public m_iNextLevelExp:number = 1000000;
	public m_fLastRefreshShopTime :number = 0;
	public m_bFailBuy:boolean = false;
	public m_iCurrentTotalProft:number = 0;
    public m_kMaxProfit:number = 0;

	public init():void
	{
		this.OnLevelUp(this.m_iLevel);
		for (var i = 0; i < 5; i++)
		{
			this.m_kValidBuyItem.push(i+1);
		}
		this.RefreshGoodsList();
	}

	public RefreshGoodsList()
	{
		for (var i = 0; i < Consts.GAME_SHOPGRID_WIDTH; i ++)
			for (var j = 0; j < Consts.GAME_SHOPGRID_HEIGHT; j ++)
			{
				var	kGoodsInfo:TGoodsInfo = this.m_kGoodsList[j * Consts.GAME_SHOPGRID_WIDTH + i];
				if (kGoodsInfo == null)
					this.m_kGoodsList[j * Consts.GAME_SHOPGRID_WIDTH + i] = new TGoodsInfo();
				kGoodsInfo =this.m_kGoodsList[j * Consts.GAME_SHOPGRID_WIDTH + i];
				var data =  JxGameConfig.m_kGameConfig;
				var player = JxGameConfig.m_kPlayerLevel;
				var index = GameUtils.RandomIntRange(0, this.m_kValidBuyItem.length);
				var itemid = this.m_kValidBuyItem[index];
				var item = data[itemid -1];	
				kGoodsInfo.fRoom = item.ocupy;
				kGoodsInfo.iItemID = itemid;
				var profit = item.profit;
				var iPrice = GameUtils.RandomIntRange(item.price * ( (item.minprofit / 100)) , item.price * ((item.maxprofit / 100)));
				kGoodsInfo.iRealPrice = iPrice;
				kGoodsInfo.iRestCount = item.count;
				kGoodsInfo.strName = item.desc;
			}

	}
	public OnUpdate(fCurrentTime:number) :void
	{
		var bNeedRefresh = true;
		var data =  JxGameConfig.m_kGameConfig;
			for (var i in this.m_kGoodsList)
			{
				var	kGoodsInfo:TGoodsInfo = this.m_kGoodsList[i];
				if (kGoodsInfo.iRestCount > 0 )
				{
					if(data[kGoodsInfo.iItemID - 1].price > kGoodsInfo.iRealPrice)
					{
						bNeedRefresh = false;
						break;
					}
				}
			}
		if (bNeedRefresh == true)
		{
			if (this.m_bFailBuy == false)
				{
					var iAddition =  Math.floor(this.m_iCurrentTotalProft *  0.5);
					GameSceneLayer.getInstance().ShowTips("恭喜！全赚，额外获得50%的收益! +" + iAddition, 1.5);
					this.m_iMoney += iAddition;
				}
		}
	if (fCurrentTime >= this.m_fLastRefreshShopTime + 60)
		{
			bNeedRefresh = true;
		}
		

		if(bNeedRefresh)
		{
			if (this.m_kMaxProfit < this.m_iCurrentTotalProft)
				this.m_kMaxProfit = this.m_iCurrentTotalProft;

			this.m_bFailBuy = false;
			this.m_iCurrentTotalProft = 0;
			this.RefreshGoodsList();
			this.m_fLastRefreshShopTime = fCurrentTime;
			GameSceneLayer.getInstance().RefreshGrid();
		}

	}
	public AddExp(iExp:number) :void
	{
		this.m_iCurExp += iExp;

		if (this.m_iCurExp >= this.m_iNextLevelExp)
		{
			this.m_iLevel ++;
			this.m_iCurExp -= this.m_iNextLevelExp;
			this.OnLevelUp(this.m_iLevel); 
		}
	}
	
	
	public BuyGoods(iGoodsIndex:number) :number
	{
		if (this.m_kGoodsList.length <= iGoodsIndex)
			return 0;
		var item = this.m_kGoodsList[iGoodsIndex];
		if (this.m_iCurRoomCount >= this.m_iMaxRoomCount)
			return 0;
		if (item.iRestCount <= 0)
			return 0;
		//if (this.CostMoney(item.iRealPrice) == false)
			//return 0;

		item.iRestCount -- ;
		this.m_iCurRoomCount += item.fRoom;
		var kSelling:TSellingItem = this.m_kBuyedGoodsList[item.iItemID];
		if (kSelling == null)
		{
			this.m_kBuyedGoodsList[item.iItemID] = new TSellingItem();

			kSelling = this.m_kBuyedGoodsList[item.iItemID];
			kSelling.kPriceList = new Array<number>();
		}
		
		kSelling.iItemId = item.iItemID;
		kSelling.iCount ++;
		var data =  JxGameConfig.m_kGameConfig;
		var profit = data[item.iItemID - 1].price - item.iRealPrice;
		kSelling.kPriceList.push( profit) ;
		if (profit < 0)
			this.m_bFailBuy = true;
		this.m_iCurrentTotalProft += profit;
		


		return profit;

	}

	public OnLevelUp(iNewLevel:number) :void 
	{
		this.m_iNextLevelExp = JxGameConfig.m_kPlayerLevel[this.m_iLevel - 1].iLevelUpCost;
		this.m_iMaxRoomCount = 1000;//JxGameConfig.m_kPlayerLevel[this.m_iLevel - 1].iMaxOcupy;
		this.m_kValidBuyItem.push(JxGameConfig.m_kPlayerLevel[this.m_iLevel - 1].strNewGoods);
		GameSceneLayer.getInstance().Refresh();
	}
	public CostMoney(iCost:number) : boolean
	{
		if (this.m_iMoney < iCost)
			return false;
		
		this.m_iMoney -= iCost;
		return true;
	}
	public CheckMoney(iCost:number) : boolean
	{
		if (this.m_iMoney < iCost)
			return false;
		return true;
	}
}