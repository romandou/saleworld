/**
 * Created by ismole on 14-8-8.
 */
class  GameControl extends egret.Sprite
{

    private static _gameControl:GameControl = null;
    public static getInstance():GameControl {

        if(!GameControl._gameControl){
            GameControl._gameControl = new GameControl();
        }
        return GameControl._gameControl;
    }

    private currStage:egret.DisplayObjectContainer;

    private  startgame:StartGameLayer;
    private  gameScene:GameSceneLayer;



    public constructor()
    {
        super();
        this.startgame= new StartGameLayer();
        this.gameScene = GameSceneLayer.getInstance();
    }

    public setStageHandler(stage:egret.DisplayObjectContainer):void
    {
        this.currStage = stage;

    }
    public startGameHandler():void
    {
        if(this.gameScene && this.gameScene.parent){
            GameUtils.removeChild(this.gameScene)
        }
        this.currStage.addChild(this.startgame);
        GameApp.xia.visible = true;
    }

    public onGameSceneHandler():void
    {
        if (GameApp.s_GameCore == null)
        {
            GameApp.s_GameCore = new JxGameCore();
            GameApp.s_GameCore.init();
        }

        if(this.startgame && this.startgame.parent){
            GameUtils.removeChild(this.startgame)
        }
        this.currStage.addChild(this.gameScene);

        GameApp.xia.visible = false;
    }
    public Refresh():void 
    {
        this.gameScene.Refresh();
    }

}