class JxGameCore {
	public constructor() 
	{
		this.init();
	}
	public m_fCurrentTime : number = 0;
	public m_fLastAddMoneyTime :number = 0;
	public init():void 
	{
		this.m_kPlayer = new JxPlayer();
		this.m_kPlayer.init();
		//创建一个计时器对象
       var timer:egret.Timer = new egret.Timer(50,0);
       //注册事件侦听器
        timer.addEventListener(egret.TimerEvent.TIMER,this.OnUpdate,this);
        //timer.addEventListener(egret.TimerEvent.TIMER_COMPLETE,this.timerComFunc,this);
        //开始计时
        timer.start();


	}
	public m_kPlayer:JxPlayer;

	private OnUpdate():void
	{
		this.m_fCurrentTime += 0.05;
		var bChanged:boolean = false;

		for (var iItemId in this.m_kPlayer.m_kBuyedGoodsList)
		{
			var kSelling = this.m_kPlayer.m_kBuyedGoodsList[iItemId];
			if (kSelling.iCount <= 0)
			{
				kSelling.fLastSellTime = 0;
				continue;
			}
			var item = JxGameConfig.m_kGameConfig[kSelling.iItemId - 1];
			if (item == null) 
				continue;
			if (kSelling.fLastSellTime == 0) 
				kSelling.fLastSellTime = this.m_fCurrentTime;

			if (kSelling.fLastSellTime + item.selltime > this.m_fCurrentTime)
				continue;

			var iProfit = kSelling.kPriceList.shift();
			var price = item.price;
			this.m_kPlayer.m_iMoney += iProfit;
			if (this.m_kPlayer.m_iMoney < 0)
				this.m_kPlayer.m_iMoney = 0;
			this.m_kPlayer.m_iCurRoomCount -= item.ocupy;
			this.m_kPlayer.AddExp(item.exp);

			GameSceneLayer.getInstance().AddProfitInfo(iProfit);

			
			kSelling.iCount --;
			kSelling.fLastSellTime = this.m_fCurrentTime;
			bChanged = true;
		}

		if (bChanged)
		{
			GameControl.getInstance().Refresh();
		}
		this.m_kPlayer.OnUpdate(this.m_fCurrentTime);

		GameSceneLayer.getInstance().OnUpdate();
		
	}
}